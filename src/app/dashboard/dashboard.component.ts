import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, from } from 'rxjs';
import * as userActions from '../userstore/user.actions'
import {ApiserviceService} from '../service/apiservice.service'
import {Usermodel} from '../userstore/user.model'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  user$: Observable<Usermodel>;

  constructor(private store:Store<any>,public apiservice:ApiserviceService) { 
   
  }
  ngOnInit() {
    this.store.dispatch({ type: userActions.ADD_DATA, payload:[]});
    this.user$ =this.store.select('userlist')
  }
  serachvalue(value) {
    let data=(value) ? this.apiservice.getuser(value) : []
    this.store.dispatch({ type: userActions.ADD_DATA, payload:data});
  }
  saveid(id){
    this.store.dispatch({ type: userActions.ADD_ID, payload:id});
  }

}
