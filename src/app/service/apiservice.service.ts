import { Injectable } from '@angular/core';
import  *  as  data  from  '../../assets/json/userdata.json';

@Injectable({
  providedIn: 'root'
})
export class ApiserviceService {

  constructor() { }

  getuser(searchText){
    let value=(searchText) ? data.user.filter(res=>{
     return res.name.toLowerCase().includes(searchText.toLowerCase()) || res.email.toLowerCase().includes(searchText.toLowerCase()) || res.address.toLowerCase().includes(searchText.toLowerCase())
    }) : []
    return value
  }
}
