import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, from } from 'rxjs';
import * as userActions from '../userstore/user.actions'

import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  userdetail$: Observable<any>;
  userid: Observable<any>
  constructor(private store:Store<any>,private route: ActivatedRoute) { 
   
  }

  ngOnInit() {
   this.store.select('userid').subscribe(res=>{
        this.store.dispatch({ type: userActions.GET_DATA, payload:res});
   });
   this.store.select('userlist').subscribe(res=>{
    this.userdetail$=res[0]
    })
  }

}
