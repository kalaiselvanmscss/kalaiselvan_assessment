
import * as userActions from './user.actions'

export function userReducer(state = [], action: userActions.UserActions) {
  switch (action.type) {
    case userActions.ADD_DATA:
      return action.payload;
    case userActions.GET_DATA:
      return state.filter(item => item.id == action.payload);
    default:
        return state;
    }
  }

  
export function useridReducer(state = [], action: userActions.UserActions) {
  switch (action.type) {
    case userActions.GET_ID:
      return state;
    case userActions.ADD_ID:
      return action.payload;
    default:
        return state;
    }
  }