import { Action } from '@ngrx/store';

export const ADD_DATA = 'ADD_DATA';
export const GET_DATA = 'GET_DATA';
export const ADD_ID = 'ADD_ID';
export const GET_ID = 'GET_ID';

export class ADDDATA implements Action {
    readonly type = ADD_DATA;
  
    constructor(public payload: { }) {}
  }
  export class GETDATA implements Action {
    readonly type = GET_DATA;
  
    constructor(public payload: { }) {}
  }
  export class ADDID implements Action {
    readonly type = ADD_ID;
  
    constructor(public payload: { }) {}
  }
  export class GETID implements Action {
    readonly type = GET_ID;
  
    constructor(public payload: { }) {}
  }
  export type UserActions = ADDDATA | GETDATA | ADDID | GETID;
