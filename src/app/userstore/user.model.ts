export interface Usermodel {
  id: number,
  name: string,
  email: string,
  phone: string,
  imageUrl:string,
  address: string
}

export interface Userid {
  id: number,
}